Particles In Box
================
Программа, созданная для визуализации и исследования распределения Максвелла.

Разработана в `НИУ МИЭТ <https://miet.ru/>`_ под руководством профессора кафедры общей физики `Гайдукова Геннадия Николаевича <https://miet.ru/person/44517>`_.
